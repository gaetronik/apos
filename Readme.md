# APOS

APOS stands for Acme Proxy on Steroids.

## Goals

This server aims at being a proxy with it's own API to DNS providers, allowing fine grained permissions using [biscuits](https://biscuitsec.org).
Since lots of DNS providers limit rights scope to zone, not records

## How to use

You'll need the biscuit cli; `cargo install biscuit`

### Generate biscuit keys

```shell
biscuit keypair --only-private-key > private_key
chmod 600 private_key
biscuit keypair --from-private-key-file private_key --only-public-key > public_key
```

### Generate biscuit for client

```shell
biscuit generate --private-key-file private_key - > biscuit <<EOF
domain("example.org"); // grant rights for domain and its subdomains
type("example.org","TXT"); // to specific type, for all type use "*"
acme("test.example.net"); // Grant right for acme_challenge for specific domain
ressource_record("record.example.org","SSHFP"); // Grant right to edit SSHFP records for record.example.org
EOF
```
The `acme` and `ressource_record` stanza are self explainatory, the `domain` and `type` stanzas needs a bit more explainations.
`domain` grants access for `type` records of both `domain` it self and subdomains. Type `*` means all provider supported types. Multiple type for a subdomain can be specified.

### Start the server

```
ROCKET_BISCUIT_PUBLIC_KEY=`cat public_key` APOS_PROVIDER=echo cargo run
```

### Call the API

Lets use [httpie](https://httpie.org) for the example

```shell
http POST :8000/api/v1/_acme-challenge.test.example.net x-api-key:$(cat biscuit) nstype="TXT" value[0]="DNSVERIFICATIONCODE" -v
```

## API Documentation

Just one endpoint at the moment using `POST` `/api/v1/<domain>` with body in JSON with following content

```json
{
	"ttl": 3600,
	"nstype": "TXT",
	"value": [ "first_value", "second_value"]
}
```
ttl is optional, default depends on provider

## Supported providers

* Route53 with `APOS_PROVIDER=route53` and when builded with feature route53
* PowerDNS with `APOS_PROVIDER=powerdns` and `APOS_POWERDNS_URL`, `APOS_POWERDNS_KEY` and optionally `APOS_POWERDNS_SERVER` set. 
