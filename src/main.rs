#[macro_use]
extern crate rocket;
use rocket::fairing::AdHoc;
use rocket::http::Status;
use rocket::request::{FromRequest, Outcome, Request};
use rocket::response::status;
use rocket::serde::{json::Json, Deserialize};
use rocket::State;
extern crate biscuit_auth as biscuit;
use biscuit::{Biscuit, PublicKey};
mod dns;
use crate::dns::{delete_dns, parse_config, update_dns, DnsUpdate, ProviderConfig};
#[cfg(test)]
use core::time::Duration;

#[derive(Debug, Deserialize)]
#[serde(crate = "rocket::serde")]
struct AppConfig {
    biscuit_public_key: String,
}

#[derive(Debug)]
struct ApiKey(Biscuit);

#[derive(Debug)]
enum ApiKeyError {
    Missing,
    Invalid,
}

async fn create_authorizer() -> biscuit::Authorizer<'static> {
    let mut auth = biscuit::Authorizer::new().unwrap();
    auth.add_code(include_str!("authorizer.dl")).unwrap();
    auth
}

fn parse_biscuit<'r>(key: &'r str, public_key: &'r str) -> Result<Biscuit, ApiKeyError> {
    let mut _biscuit: &'r Biscuit;
    match Biscuit::from_base64(key, |_| {
        PublicKey::from_bytes(&hex::decode(public_key).unwrap()).unwrap()
    }) {
        Ok(b) => Ok(b),
        Err(_) => Err(ApiKeyError::Invalid),
    }
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for ApiKey {
    type Error = ApiKeyError;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let config = req.rocket().state::<AppConfig>().unwrap();
        let key = match req.headers().get_one("x-api-key") {
            None => return Outcome::Failure((Status::BadRequest, ApiKeyError::Missing)),
            Some(key) => key,
        };
        match parse_biscuit(key, &config.biscuit_public_key) {
            Ok(b) => Outcome::Success(ApiKey(b)),
            Err(e) => Outcome::Failure((Status::BadRequest, e)),
        }
    }
}

#[derive(Debug, PartialEq)]
struct AuthenticationError(String);

impl From<biscuit::error::Token> for AuthenticationError {
    fn from(err: biscuit::error::Token) -> AuthenticationError {
        AuthenticationError(format!("{}", err))
    }
}

fn is_valid(
    key: &ApiKey,
    dns_update: &DnsUpdate<'_>,
    domain: &str,
    authorizer: &biscuit::Authorizer<'static>,
) -> Result<(), AuthenticationError> {
    let ApiKey(biscuit) = key;
    let mut verifier = authorizer.clone();
    verifier.add_token(biscuit)?;
    // Add facts from request
    let mut request_record: biscuit::builder::Fact = "request_record({req})".try_into()?;
    request_record.set("req", domain)?;
    verifier.add_fact(request_record)?;
    let mut request_type: biscuit::builder::Fact = "request_type({req})".try_into()?;
    request_type.set("req", dns_update.nstype.to_ascii_uppercase())?;
    verifier.add_fact(request_type)?;
    //verifier.print_world();
    #[cfg(test)]
    let limits = biscuit::AuthorizerLimits {
        max_time: Duration::from_secs(5),
        ..Default::default()
    };
    #[cfg(not(test))]
    let limits = biscuit::AuthorizerLimits::default();
    match verifier.authorize_with_limits(limits) {
        Ok(_) => Ok(()),
        Err(e) => Err(AuthenticationError(format!("{}", e))),
    }
}

#[post("/api/v1/<domain>", format = "json", data = "<dns_update>")]
async fn modify_domain(
    domain: String,
    dns_update: Json<DnsUpdate<'_>>,
    key: ApiKey,
    dns_config: &State<ProviderConfig>,
    base_authorizer: &State<biscuit::Authorizer<'static>>,
) -> status::Custom<String> {
    let dns_update: DnsUpdate = dns_update.into_inner();
    match is_valid(&key, &dns_update, &domain, base_authorizer) {
        Ok(_) => match update_dns(&domain, &dns_update, dns_config).await {
            Ok(msg) => status::Custom(Status::Ok, msg),
            Err(e) => status::Custom(Status::InternalServerError, format!("Error {:?}", e)),
        },
        Err(AuthenticationError(e)) => status::Custom(Status::Unauthorized, e),
    }
}

#[delete("/api/v1/<domain>", format = "json", data = "<dns_update>")]
async fn delete_domain(
    domain: String,
    dns_update: Json<DnsUpdate<'_>>,
    key: ApiKey,
    dns_config: &State<ProviderConfig>,
    base_authorizer: &State<biscuit::Authorizer<'static>>,
) -> status::Custom<String> {
    let dns_update: DnsUpdate = dns_update.into_inner();
    match is_valid(&key, &dns_update, &domain, base_authorizer) {
        Ok(_) => match delete_dns(&domain, &dns_update, dns_config).await {
            Ok(msg) => status::Custom(Status::Ok, msg),
            Err(e) => status::Custom(Status::InternalServerError, format!("Error {:?}", e)),
        },
        Err(AuthenticationError(e)) => status::Custom(Status::Unauthorized, e),
    }
}
#[launch]
fn rocket() -> _ {
    rocket::build()
        .attach(AdHoc::config::<AppConfig>())
        .attach(AdHoc::try_on_ignite(
            "DNS Provider Config",
            |rocket| async { Ok(rocket.manage(parse_config().await)) },
        ))
        .attach(AdHoc::try_on_ignite("Authorizer Config", |rocket| async {
            Ok(rocket.manage(create_authorizer().await))
        }))
        .mount("/", routes![modify_domain, delete_domain])
}

#[cfg(test)]
mod tests {
    use crate::{create_authorizer, is_valid, ApiKey, DnsUpdate};
    use biscuit_auth::{Biscuit, KeyPair};
    use futures::executor::block_on;
    fn test_biscuit() -> Biscuit {
        let root = KeyPair::new();
        let mut builder = Biscuit::builder(&root);
        builder
            .add_authority_fact("domain(\"example.org\")")
            .unwrap();
        builder
            .add_authority_fact("type(\"example.org\", \"TXT\")")
            .unwrap();
        builder.build().unwrap()
    }
    fn test_biscuit_acme() -> Biscuit {
        let root = KeyPair::new();
        let mut builder = Biscuit::builder(&root);
        builder
            .add_authority_fact("acme(\"test.example.org\")")
            .unwrap();
        builder.build().unwrap()
    }
    #[test]
    fn test_auth_ok() {
        let authorizer = block_on(create_authorizer());
        let biscuit = test_biscuit();
        assert_eq!(
            is_valid(
                &ApiKey(biscuit),
                &DnsUpdate {
                    value: vec!["test \"truc\""],
                    nstype: "TXT",
                    ttl: None,
                    priority: None
                },
                "sub.example.org",
                &authorizer
            ),
            Ok(())
        )
    }
    #[test]
    fn test_auth_nok_wrong_type() {
        let authorizer = block_on(create_authorizer());
        let biscuit = test_biscuit();
        assert!(is_valid(
            &ApiKey(biscuit),
            &DnsUpdate {
                value: vec!["test \"truc\""],
                nstype: "A",
                ttl: None,
                priority: None
            },
            "sub.example.org",
            &authorizer
        )
        .is_err())
    }
    #[test]
    fn test_auth_nok_wrong_sub() {
        let authorizer = block_on(create_authorizer());
        let biscuit = test_biscuit();
        assert!(is_valid(
            &ApiKey(biscuit),
            &DnsUpdate {
                value: vec!["test \"truc\""],
                nstype: "TXT",
                ttl: None,
                priority: None
            },
            "subexample.org",
            &authorizer
        )
        .is_err())
    }
    #[test]
    fn test_auth_acme_ok() {
        let authorizer = block_on(create_authorizer());
        let biscuit = test_biscuit_acme();
        assert!(is_valid(
            &ApiKey(biscuit),
            &DnsUpdate {
                value: vec!["test \"truc\""],
                nstype: "TXT",
                ttl: None,
                priority: None
            },
            "_acme-challenge.test.example.org",
            &authorizer
        )
        .is_ok())
    }
    #[test]
    fn test_auth_acme_nok() {
        let authorizer = block_on(create_authorizer());
        let biscuit = test_biscuit_acme();
        assert!(is_valid(
            &ApiKey(biscuit),
            &DnsUpdate {
                value: vec!["test \"truc\""],
                nstype: "TXT",
                ttl: None,
                priority: None
            },
            "test.example.org",
            &authorizer
        )
        .is_err())
    }
}
