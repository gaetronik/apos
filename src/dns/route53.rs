use crate::{dns::DNSError, DnsUpdate};

// A function to check if a zone might contain a record
// zone: A zone as given from route53 crate
// domain: A &str for the record to test
fn check_suffix(zone: aws_sdk_route53::model::HostedZone, domain: &str) -> Option<AwsZone> {
    let domain = &format!("{}.", domain);
    match zone.name {
        Some(ref name) => {
            if domain.ends_with(name) {
                Some(AwsZone {
                    name: name.to_string(),
                    id: zone.id?,
                })
            } else {
                None
            }
        }
        None => None,
    }
}

#[derive(Debug, Clone, PartialEq)]
struct AwsZone {
    name: String,
    id: String,
}

fn sort_zone(zones: &mut Vec<AwsZone>) -> () {
    zones.sort_by(|a, b| b.name.len().cmp(&a.name.len()));
}

async fn get_root_zone(client: aws_sdk_route53::Client, domain: &str) -> Result<AwsZone, DNSError> {
    let listzone = match client.list_hosted_zones_by_name().send().await {
        Ok(listzone) => listzone,
        Err(_) => {
            return Err(DNSError("Error fetching zone".to_string()));
        }
    };
    let listzone = match listzone.hosted_zones {
        Some(l) => l,
        None => return Err(DNSError("No zone found".to_string())),
    };
    let mut matchingzones = listzone
        .into_iter()
        .filter_map(|e| check_suffix(e, domain))
        .collect::<Vec<AwsZone>>();
    sort_zone(&mut matchingzones);
    match matchingzones.first() {
        None => Err(DNSError("No zone found".to_string())),
        Some(e) => Ok(AwsZone {
            name: String::from(e.name.as_str()),
            id: String::from(e.id.as_str()),
        }),
    }
}

pub async fn update_dns(
    domain: &str,
    update: &DnsUpdate<'_>,
    config: &aws_config::Config,
) -> Result<String, DNSError> {
    change_zone(
        domain,
        update,
        config,
        aws_sdk_route53::model::ChangeAction::Upsert,
    )
    .await
}

pub async fn delete_dns(
    domain: &str,
    update: &DnsUpdate<'_>,
    config: &aws_config::Config,
) -> Result<String, DNSError> {
    change_zone(
        domain,
        update,
        config,
        aws_sdk_route53::model::ChangeAction::Delete,
    )
    .await
}

pub async fn change_zone(
    domain: &str,
    update: &DnsUpdate<'_>,
    config: &aws_config::Config,
    modification_type: aws_sdk_route53::model::ChangeAction,
) -> Result<String, DNSError> {
    let client = aws_sdk_route53::Client::new(config);
    let root_zone = get_root_zone(client.clone(), domain).await?;
    let resource_records = update
        .value
        .clone()
        .into_iter()
        .map(|e| match update.priority {
            None => aws_sdk_route53::model::ResourceRecord::builder()
                .value(format!("\"{}\"", e))
                .build(),
            Some(p) => aws_sdk_route53::model::ResourceRecord::builder()
                .value(format!("\"{} {}\"", p, e))
                .build(),
        })
        .collect::<Vec<aws_sdk_route53::model::ResourceRecord>>();
    let resource_record_set = aws_sdk_route53::model::ResourceRecordSet::builder()
        .name(domain)
        .r#type(aws_sdk_route53::model::RrType::from(update.nstype))
        .set_resource_records(Some(resource_records))
        .ttl(update.ttl.unwrap_or(3600))
        .build();
    let change = aws_sdk_route53::model::Change::builder()
        .action(modification_type)
        .resource_record_set(resource_record_set)
        .build();
    let change_batch = aws_sdk_route53::model::ChangeBatch::builder()
        .comment("Add from APOS")
        .changes(change)
        .build();
    match client
        .change_resource_record_sets()
        .hosted_zone_id(root_zone.id)
        .change_batch(change_batch)
        .send()
        .await
    {
        Ok(ret) => Ok(format!("AWS update on {} got {:?}", root_zone.name, ret)),
        Err(e) => Err(DNSError(format!("Error: {:?}", e))),
    }
}

#[cfg(test)]
mod tests {
    use crate::dns::route53::{check_suffix, sort_zone, AwsZone};
    use aws_sdk_route53::model::HostedZone;
    #[test]
    fn test_sort_zone() {
        let zone1 = AwsZone {
            name: "short".to_string(),
            id: "not important".to_string(),
        };
        let zone2 = AwsZone {
            name: "verylong".to_string(),
            id: "not important".to_string(),
        };
        let zone3 = AwsZone {
            name: "longer".to_string(),
            id: "not important".to_string(),
        };
        let mut zones = vec![zone1.clone(), zone2.clone(), zone3.clone()];
        sort_zone(&mut zones);
        assert_eq!(zones, vec![zone2, zone3, zone1])
    }
    #[test]
    fn test_suffix() {
        let zone = HostedZone::builder()
            .id("test")
            .name("example.org.")
            .build();
        assert!(check_suffix(zone, "test.example.org").is_some())
    }
}
