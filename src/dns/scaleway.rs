use crate::dns::{DNSError, DnsUpdate};

use rocket::serde::Deserialize;
use scaleway_api_rs::apis::configuration::ApiKey;
use scaleway_api_rs::apis::configuration::Configuration as ScalewayConfig;
use scaleway_api_rs::apis::dns_zones_api::list_dns_zones;
use scaleway_api_rs::apis::dns_zones_api::ListDnsZonesError;

#[derive(Debug, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct InternalScalewayConfig {
    key: String,
}

pub fn get_config() -> ScalewayConfig {
    let internal_config = envy::prefixed("APOS_SCALEWAY_")
        .from_env::<InternalScalewayConfig>()
        .unwrap();
    let mut config = ScalewayConfig::new();
    config.api_key = Some(ApiKey {
        prefix: None,
        key: internal_config.key,
    });
    //config.base_path="http://localhost:5000".to_owned();
    config.client = reqwest::Client::new();
    config
}

impl From<scaleway_api_rs::apis::Error<ListDnsZonesError>> for DNSError {
    fn from(err: scaleway_api_rs::apis::Error<ListDnsZonesError>) -> DNSError {
        DNSError(format!("{:?}", err))
    }
}

fn create_zone_domain(subdomain: &Option<String>, domain: &Option<String>) -> Option<String> {
    match domain {
        None => None,
        Some(domain) => match subdomain {
            None => Some(domain.to_string()),
            Some(subdomain) => match subdomain.len() {
                0 => Some(domain.to_string()),
                _ => Some(format!("{}.{}", subdomain, domain)),
            },
        },
    }
}

fn check_suffix(
    zone: &scaleway_api_rs::models::ScalewayDomainV2beta1DnsZone,
    domain: &str,
) -> bool {
    let zone_domain = create_zone_domain(&zone.subdomain, &zone.domain);
    match zone_domain {
        None => false,
        Some(zone_domain) => domain.ends_with(&zone_domain),
    }
}

fn sort_zone(zones: &mut Vec<scaleway_api_rs::models::ScalewayDomainV2beta1DnsZone>) {
    zones.sort_by(|a, b| {
        create_zone_domain(&b.subdomain, &b.domain)
            .unwrap_or_default()
            .len()
            .cmp(
                &create_zone_domain(&a.subdomain, &a.domain)
                    .unwrap_or_default()
                    .len(),
            )
    })
}

async fn get_root_zone(
    domain: &str,
    config: &ScalewayConfig,
) -> Result<scaleway_api_rs::models::ScalewayDomainV2beta1DnsZone, DNSError> {
    let zones = match list_dns_zones(config, None, None, None, None, None, None, None).await {
        Ok(zones) => zones,
        Err(e) => {
            return Err(DNSError(e.to_string()));
        }
    };
    let zones = match zones.dns_zones {
        None => return Err(DNSError("No zone found".to_string())),
        Some(zones) => zones,
    };
    let mut matching_zones = zones
        .into_iter()
        .filter(|z| check_suffix(z, domain))
        .collect::<Vec<scaleway_api_rs::models::ScalewayDomainV2beta1DnsZone>>();
    sort_zone(&mut matching_zones);
    match matching_zones.first() {
        None => Err(DNSError("No zone match".to_string())),
        Some(zone) => Ok(zone.clone()),
    }
}

fn nstype_to_scaleway_record_type(
    nstype: &str,
) -> scaleway_api_rs::models::ScalewayDomainV2beta1RecordType {
    match nstype.to_uppercase().as_str() {
        "A" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::A,
        "AAAA" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::AAAA,
        "CNAME" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::CNAME,
        "TXT" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::TXT,
        "SRV" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::SRV,
        "TLSA" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::TLSA,
        "MX" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::MX,
        "NS" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::NS,
        "PTR" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::PTR,
        "CAA" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::CAA,
        "ALIAS" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::ALIAS,
        "LOC" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::LOC,
        "SSHFP" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::SSHFP,
        "HINFO" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::HINFO,
        "RP" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::RP,
        "URI" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::URI,
        "DS" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::DS,
        "NAPTR" => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::NAPTR,
        _ => scaleway_api_rs::models::ScalewayDomainV2beta1RecordType::Unknown,
    }
}

pub async fn update_dns(
    domain: &str,
    update: &DnsUpdate<'_>,
    config: &ScalewayConfig,
) -> Result<String, DNSError> {
    let zone = get_root_zone(domain, config).await;
    let zone_domain = match zone {
        Err(e) => return Err(e),
        Ok(zone) => create_zone_domain(&zone.subdomain, &zone.domain).unwrap_or_default(),
    };
    let records = update
        .value
        .clone()
        .into_iter()
        .map(|e| {
            let mut record = scaleway_api_rs::models::ScalewayDomainV2beta1Record::new();
            record.priority = update.priority;
            record.ttl = match update.ttl {
                Some(_) => update.ttl,
                None => Some(3600),
            };
            record.data = Some(e.to_string());
            let mut domain = domain.to_string();
            domain.truncate(domain.len() - zone_domain.len() - 1);
            record.name = Some(domain.to_string());
            record._type = Some(nstype_to_scaleway_record_type(update.nstype));
            //record.id = Some("f60a11d4-dc76-11ec-84c0-df196b67caa1".to_string());
            record
        })
        .collect::<Vec<scaleway_api_rs::models::ScalewayDomainV2beta1Record>>();
    let mut payload = scaleway_api_rs::models::UpdateDnsZoneRecordsRequest::new();
    let mut change = scaleway_api_rs::models::ScalewayDomainV2beta1RecordChange::new();
    let mut changeadd = scaleway_api_rs::models::ScalewayDomainV2beta1RecordChangeAdd::new();
    changeadd.records = Some(records);
    change.add = Some(Box::new(changeadd));
    payload.changes = Some(vec![change]);
    match scaleway_api_rs::apis::records_api::update_dns_zone_records(
        &config,
        &zone_domain,
        payload,
    )
    .await
    {
        Ok(_) => Ok("Update Ok".to_string()),
        Err(e) => Err(DNSError(e.to_string())),
    }
}
pub async fn delete_dns(
    domain: &str,
    update: &DnsUpdate<'_>,
    config: &ScalewayConfig,
) -> Result<String, DNSError> {
    let zone = get_root_zone(domain, config).await;
    let zone_domain = match zone {
        Err(e) => return Err(e),
        Ok(zone) => create_zone_domain(&zone.subdomain, &zone.domain).unwrap_or_default(),
    };
    let mut payload = scaleway_api_rs::models::UpdateDnsZoneRecordsRequest::new();
    let changes = update
        .value
        .clone()
        .into_iter()
        .map(|e| {
            let mut change = scaleway_api_rs::models::ScalewayDomainV2beta1RecordChange::new();
            let mut changedel =
                scaleway_api_rs::models::ScalewayDomainV2beta1RecordChangeDelete::new();
            let mut id_fields =
                scaleway_api_rs::models::ScalewayDomainV2beta1RecordChangeSetIdFields::new();
            let mut domain = domain.to_string();
            domain.truncate(domain.len() - zone_domain.len() - 1);
            id_fields.name = Some(domain.to_string());
            id_fields._type = Some(nstype_to_scaleway_record_type(update.nstype));
            id_fields.data = Some(e.to_string());
            id_fields.ttl = update.ttl;
            changedel.id_fields = Some(Box::new(id_fields));
            change.delete = Some(Box::new(changedel));
            change
        })
        .collect::<Vec<scaleway_api_rs::models::ScalewayDomainV2beta1RecordChange>>();
    payload.changes = Some(changes);
    //let mut config = config.clone();
    //config.base_path="http://localhost:5000".to_owned();
    match scaleway_api_rs::apis::records_api::update_dns_zone_records(
        &config,
        &zone_domain,
        payload,
    )
    .await
    {
        Ok(_) => Ok("Delete Ok".to_string()),
        Err(e) => Err(DNSError(e.to_string())),
    }
}
