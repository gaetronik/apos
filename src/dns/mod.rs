use rocket::serde::Deserialize;
use std::env;
use std::ffi::OsString;
#[cfg(feature = "powerdns")]
mod powerdns;
#[cfg(feature = "route53")]
mod route53;
#[cfg(feature = "scaleway")]
mod scaleway;

#[derive(Debug)]
pub struct DNSError(String);

pub async fn update_dns(
    domain: &str,
    update: &DnsUpdate<'_>,
    dns_config: &ProviderConfig,
) -> Result<String, DNSError> {
    match dns_config {
        #[cfg(feature = "route53")]
        ProviderConfig::Route53(config) => route53::update_dns(domain, update, config).await,
        #[cfg(feature = "powerdns")]
        ProviderConfig::PowerDns(config) => powerdns::update_dns(domain, update, config).await,
        #[cfg(feature = "scaleway")]
        ProviderConfig::Scaleway(config) => scaleway::update_dns(domain, update, config).await,
        ProviderConfig::EchoConfig => {
            let ret = format!("Receive update for {}, with values: {:#?}", domain, update);
            Ok(ret)
        }
    }
}

pub async fn delete_dns(
    domain: &str,
    update: &DnsUpdate<'_>,
    dns_config: &ProviderConfig,
) -> Result<String, DNSError> {
    match dns_config {
        #[cfg(feature = "route53")]
        ProviderConfig::Route53(config) => route53::delete_dns(domain, update, config).await,
        #[cfg(feature = "powerdns")]
        ProviderConfig::PowerDns(config) => powerdns::delete_dns(domain, update, config).await,
        #[cfg(feature = "scaleway")]
        ProviderConfig::Scaleway(config) => scaleway::delete_dns(domain, update, config).await,
        ProviderConfig::EchoConfig => {
            let ret = format!("Receive delete for {}, with values: {:#?}", domain, update);
            Ok(ret)
        }
    }
}

#[derive(Deserialize, Debug)]
#[serde(crate = "rocket::serde")]
pub struct DnsUpdate<'r> {
    pub nstype: &'r str,
    pub value: Vec<&'r str>,
    pub ttl: Option<i64>,
    pub priority: Option<i64>,
}

#[derive(Debug)]
pub enum ProviderConfig {
    #[cfg(feature = "route53")]
    Route53(aws_config::Config),
    #[cfg(feature = "powerdns")]
    PowerDns(powerdns::PowerDnsConfig),
    #[cfg(feature = "scaleway")]
    Scaleway(scaleway_api_rs::apis::configuration::Configuration),
    EchoConfig,
}

pub async fn parse_config() -> ProviderConfig {
    match env::var_os("APOS_PROVIDER") {
        Some(provider) => get_config_provider(provider).await,
        None => {
            println!("No provider given falling back to echo");
            ProviderConfig::EchoConfig
        }
    }
}

async fn get_config_provider(provider: OsString) -> ProviderConfig {
    let provider = match provider.into_string() {
        Ok(provider) => provider,
        Err(_) => "echo".to_string(),
    };
    match provider.as_str() {
        #[cfg(feature = "route53")]
        "route53" => ProviderConfig::Route53(aws_config::load_from_env().await),
        #[cfg(feature = "powerdns")]
        "powerdns" => ProviderConfig::PowerDns(powerdns::get_config()),
        #[cfg(feature = "scaleway")]
        "scaleway" => ProviderConfig::Scaleway(scaleway::get_config()),
        _ => {
            println!("Invalid provider given, falling back to echo");
            ProviderConfig::EchoConfig
        }
    }
}
