use crate::dns::{DNSError, DnsUpdate};
use reqwest::header;
use rocket::serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct PowerDnsConfig {
    url: String,
    key: String,
    #[serde(default = "default_server_id")]
    server_id: String,
}

fn default_server_id() -> String {
    "localhost".to_string()
}

#[derive(Debug, Clone, Deserialize, PartialEq)]
#[serde(crate = "rocket::serde")]
struct PowerDnsZoneResponse {
    id: String,
    name: String,
}

#[derive(Debug, Clone, Serialize)]
#[serde(crate = "rocket::serde")]
struct PowerDnsRecord {
    content: String,
    disabled: bool,
}

impl PowerDnsRecord {
    pub fn new_enabled_quoted(content: String) -> Self {
        PowerDnsRecord {
            content: format!("\"{}\"", content),
            disabled: false,
        }
    }
    pub fn new_enabled(content: String) -> Self {
        PowerDnsRecord {
            content,
            disabled: false,
        }
    }
}

#[derive(Debug, Clone, Serialize)]
#[serde(crate = "rocket::serde")]
struct PowerDnsRRset {
    name: String,
    r#type: String,
    ttl: Option<i64>,
    changetype: PowerDnsUpdateType,
    records: Vec<PowerDnsRecord>,
    comments: Option<String>,
}

#[derive(Debug, Clone, Serialize)]
#[serde(crate = "rocket::serde")]
struct PowerDnsZoneUpdate {
    rrsets: Vec<PowerDnsRRset>,
}

#[derive(Debug, Clone, Serialize)]
#[serde(crate = "rocket::serde")]
enum PowerDnsUpdateType {
    DELETE,
    REPLACE,
}

impl From<reqwest::Error> for DNSError {
    fn from(err: reqwest::Error) -> DNSError {
        DNSError(format!("{}", err))
    }
}

pub fn get_config() -> PowerDnsConfig {
    envy::prefixed("APOS_POWERDNS_")
        .from_env::<PowerDnsConfig>()
        .unwrap()
}

fn check_suffix(zone: &PowerDnsZoneResponse, domain: &str) -> bool {
    let domain = &format!("{}.", domain);
    domain.ends_with(&zone.name)
}

fn sort_zone(zones: &mut Vec<PowerDnsZoneResponse>) {
    zones.sort_by(|a, b| b.name.len().cmp(&a.name.len()));
}

async fn get_root_zone<'r>(
    client: &reqwest::Client,
    config: &PowerDnsConfig,
    record_name: &str,
) -> Result<PowerDnsZoneResponse, DNSError> {
    let zones: Vec<PowerDnsZoneResponse> = client
        .get(format!(
            "{}/api/v1/servers/{}/zones",
            config.url, config.server_id
        ))
        .send()
        .await?
        .json()
        .await?;
    let mut zones = zones
        .into_iter()
        .filter(|z| check_suffix(z, record_name))
        .collect::<Vec<PowerDnsZoneResponse>>();
    sort_zone(&mut zones);
    match zones.first() {
        None => Err(DNSError("No zone found".to_string())),
        Some(zone) => Ok(PowerDnsZoneResponse {
            name: String::from(zone.name.as_str()),
            id: String::from(zone.id.as_str()),
        }),
    }
}

pub async fn update_dns(
    domain: &str,
    update: &DnsUpdate<'_>,
    config: &PowerDnsConfig,
) -> Result<String, DNSError> {
    change_zone(domain, update, config, PowerDnsUpdateType::REPLACE).await
}

pub async fn delete_dns(
    domain: &str,
    update: &DnsUpdate<'_>,
    config: &PowerDnsConfig,
) -> Result<String, DNSError> {
    change_zone(domain, update, config, PowerDnsUpdateType::DELETE).await
}

async fn change_zone(
    domain: &str,
    update: &DnsUpdate<'_>,
    config: &PowerDnsConfig,
    update_type: PowerDnsUpdateType,
) -> Result<String, DNSError> {
    let mut headers = header::HeaderMap::new();
    let mut auth_header = header::HeaderValue::from_str(&config.key.clone()).unwrap();
    auth_header.set_sensitive(true);
    headers.insert("X-API-KEY", auth_header);
    let client = match reqwest::Client::builder().default_headers(headers).build() {
        Ok(c) => c,
        Err(e) => {
            return Err(DNSError(format!(
                "Error while creating reqwest client: {}",
                e
            )))
        }
    };
    let zone = get_root_zone(&client, config, domain).await?;
    let records = update
        .value
        .clone()
        .into_iter()
        .map(|e| {
            let e = match update.priority {
                None => e.to_string(),
                Some(p) => format!("{} {}", p, e),
            };
            match update.nstype {
                "TXT" => PowerDnsRecord::new_enabled_quoted(e.to_string()),
                _ => PowerDnsRecord::new_enabled(e.to_string()),
            }
        })
        .collect::<Vec<PowerDnsRecord>>();
    let ttl = match update_type {
        PowerDnsUpdateType::REPLACE => Some(update.ttl.unwrap_or(3600)),
        PowerDnsUpdateType::DELETE => None,
    };
    let rrset = PowerDnsRRset {
        name: format!("{}.", domain),
        r#type: update.nstype.to_string(),
        ttl,
        changetype: update_type,
        records,
        comments: None,
    };
    let zone_update = PowerDnsZoneUpdate {
        rrsets: vec![rrset],
    };
    let ret = client
        .patch(format!(
            "{}/api/v1/servers/{}/zones/{}",
            config.url, config.server_id, zone.id
        ))
        .json(&zone_update)
        .send()
        .await?
        .text()
        .await?;
    Ok(format!(
        "Receiving update to be sent to {}/api/v1/servers/{} with {} zone selected {:?} generated record {:?} got {}",
        config.url, config.server_id, config.key, zone, zone_update, ret
    ))
}

#[cfg(test)]
mod tests {
    use crate::dns::powerdns::{check_suffix, sort_zone, PowerDnsZoneResponse};
    #[test]
    fn test_sort_zone() {
        let zone1 = PowerDnsZoneResponse {
            name: "short".to_string(),
            id: "not important".to_string(),
        };
        let zone2 = PowerDnsZoneResponse {
            name: "verylong".to_string(),
            id: "not important".to_string(),
        };
        let zone3 = PowerDnsZoneResponse {
            name: "longer".to_string(),
            id: "not important".to_string(),
        };
        let mut zones = vec![zone1.clone(), zone2.clone(), zone3.clone()];
        sort_zone(&mut zones);
        assert_eq!(zones, vec![zone2, zone3, zone1])
    }
    #[test]
    fn test_suffix() {
        let zone = PowerDnsZoneResponse {
            name: "test.example.org.".to_string(),
            id: "not important".to_string(),
        };
        assert!(check_suffix(&zone, "test.example.org"))
    }
}
